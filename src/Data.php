<?php

    namespace Licenv;

    use \My\MyPdo;
    use \MyPractic\MyQuery;

    use \Licenv\Config;
    use \PDO;

class Data {
    /**
     * Инициировать ресурсы бд
     */
    public function Init() {

        // таблица учета лицензий
        $q = "CREATE TABLE IF NOT EXISTS `".  Config::LICENCE_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`tmst` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
`till_date` date NULL,
`price` int NULL,
`block` int NULL,

    KEY (`till_date`),
    KEY (`tmst`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";

        MyPdo::get()->query($q);

        // таблица платежей
        $q = "CREATE TABLE IF NOT EXISTS `".  Config::PAYMENT_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`licence` INT NOT NULL,
`tmst` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
`price` int NULL,
`howdo` int NULL,

    KEY (`tmst`),
    KEY (`licence`),

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";
        
        MyPdo::get()->query($q);
        
        // таблица ассоциированных пользователей
        $q = "CREATE TABLE IF NOT EXISTS `".  Config::ASSOCIATED_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`licence` INT NOT NULL,
`user` INT NOT NULL,

    KEY (`licence`),
    UNIQUE (`user`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";

        MyPdo::get()->query($q);
        
    }

    /**
    * Зарегистрировать лицензию
    */
    public function setLicence() {
        MyPdo::get()->query((new MyQuery(Config::LICENCE_TABLE, ['block' => 0]))-> Insert());
        return MyPdo::get()->lastInsertId();
    }

    /**
     * Продлить срок действия лицензии
     */
    public function setTill($licence, $date) {
        MyPdo::get()->query((new MyQuery(Config::LICENCE_TABLE, ['till_date' => $date]))->Update($licence));
    }

    /**
     * Установить стоимость использования
     */
    public function setPrice($licence, $price) {
        MyPdo::get()->query((new MyQuery(Config::LICENCE_TABLE, ['price' => $price]))->Update($licence));
    }

    /**
     * Получить описание лицензии
     */
    public function checkLicence($id) {
           $stmt = MyPdo::get()->query("SELECT * FROM ". Config::LICENCE_TABLE ." WHERE id = ". (int) $id."");
        if($stmt) {
               $row = $stmt -> fetch(PDO::FETCH_ASSOC);
               return $row;
        }
    }

    /**
     * Установить связь пользователя с лицензией
     */
    public function setAssociated($user, $licence) {
        if(!$this->checkAssociated($user, $licence = FALSE)) {
        MyPdo::get()->query((new MyQuery(Config::ASSOCIATED_TABLE, ['user' => $user, 'licence' => $licence]))->Insert());
        } else {
            throw new \Exception('user was associated early');
        }
    }

    /**
     * Получить лицензию для пользователя ИЛИ пользователей для лицензии
     */
    public function checkAssociated($user = false, $licence = false) {

        $clause.= ($user) ? " user = ".(int) $user." " : "";
        $clause.= ($licence) ? " licence = ".(int) $user." " : "";

        $q = "SELECT * FROM ".Config::ASSOCIATED_TABLE." WHERE $clause";
        $stmt = MyPdo::get()->query($q);

        if($stmt->rowCount()>1) { return $stmt->fetchAll(PDO::FETCH_ASSOC); }
        else { return $stmt->fetch(PDO::FETCH_ASSOC); }
    }

    /**
     * Фиксировать платеж
     */
    public function setPayment($licence, $price, $howdo = 0) {
        MyPdo::get()->query((new MyQuery(Config::PAYMENT_TABLE, ['licence' => $licence, 'price' => $price, 'howdo' => $howdo]))-> Insert());
    }
}
