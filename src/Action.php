<?php

namespace Licenv;
    
    use \Licenv\Config;
    use \Licenv\Data;
    use \MyPractic\Datef;

/**
 * Description of Action
 *
 * @author Dell
 */
class Action {

    /**
     * Зарегистрировать новую лицензию 
     */
    public function NewLicence($user) {

        $data = new Data;
        $licence = $data->setLicence();
        $data->setPrice($licence, Config::INITIAL_PRICE);
        // определим начальный срок действия
        $till_date = Datef::date_to_period(date('Y-m-d'), '+', Config::INITIAL_PERIOD_MONTH, 'm');
        $data->setTill($licence, $till_date);
        // закрепим за пользователем
        $data->setAssociated($user, $licence);
        return $licence;
    }
    /**
     * Продлить лицензию
     */
    public function Prolong($period, $licence) {    
        
                    $data = new Data;
           $check = $data->checkLicence($licence);
        if($check) {
           $till_date = Datef::date_to_period($check['till_date'], '+', $period, 'm');
           $data->setTill($check['id'], $till_date);
           } else { throw new \Exception('licence not found'); }
    }

    /** фиксировать платеж * 
     * 
     */

}
