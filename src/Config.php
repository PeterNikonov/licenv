<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Licenv;

/**
 * Description of Config
 *
 * @author Dell
 */
class Config {
    /**
     * таблица лицензии
     */
    const LICENCE_TABLE = 'env_licence';
    /**
     * таблица платежи
     */
    const PAYMENT_TABLE = 'env_payment';
    /**
     * таблица связанные пользователи
     */
    const ASSOCIATED_TABLE = 'env_associated';
    /**
     * начальная стоимость
     */
    const INITIAL_PRICE = 0;
    /**
     * начальная период
     */
    const INITIAL_PERIOD_MONTH = 1;

}
